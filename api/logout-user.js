import usersOnServer from './users-for-backend'
import options from './options'

export default token => new Promise((resolve) => {
  // test errors
  // eslint-disable-next-line
  // func()
  setTimeout(() => {
    const users = usersOnServer()
    users.forEach((user) => {
      if (user.token === token) user.token = ''
    })
    localStorage.setItem(options.usersName, JSON.stringify(users))
    resolve()
  }, 200)
})
