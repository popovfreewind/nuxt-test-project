import jwt from 'jsonwebtoken'
import usersOnServer from './users-for-backend'
import options from './options'

const getToken = user => jwt.sign({
  user: user.login,
  password: user.password
}, 'private key')

const resultFromServer = (userData) => {
  let token = null
  let status = null
  let userFound = false
  const newArr = usersOnServer()
  newArr.forEach((user) => {
    if (userFound) return
    if (user.login === userData.login) {
      if (user.password === userData.password) {
        token = getToken(userData)
        user.token = token
        userFound = true
        status = 'authenticated'
      } else status = 'wrong-password'
    } else status = 'not-exist'
  })
  if (status === 'authenticated' && token) {
    localStorage.setItem(options.usersName, JSON.stringify(newArr))
    return { status: status, token: token }
  } else return { status: status }
}

export default userData => new Promise((resolve) => {
  // test errors
  // eslint-disable-next-line
  // func()
  setTimeout(() => resolve(resultFromServer(userData)), 500)
})
