import users from './users-for-backend'
import options from './options'

const token = localStorage.getItem(options.tokenName)
const getUser = () => users().find(user => user.token === token)

export default () => new Promise((resolve) => {
  // test errors
  // eslint-disable-next-line
  // func()
  setTimeout(() => resolve(getUser()), 200)
})
