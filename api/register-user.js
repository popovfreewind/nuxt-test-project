import jwt from 'jsonwebtoken'
import usersOnServer from './users-for-backend'
import options from './options'

const getToken = user => jwt.sign({
  user: user.login,
  password: user.password
}, 'private key')

const pushToStorage = (arr, newUser, token) => {
  newUser.token = token
  arr.push(newUser)
  localStorage.setItem(options.usersName, JSON.stringify(arr))
}

const resultFromServer = (userData) => {
  const usersArray = usersOnServer()
  if (usersArray.length === 0) {
    // push first user to server
    const token = getToken(userData)
    pushToStorage([], userData, token)
    return { status: 'registered', token: token }
  } else {
    const userExist = usersArray.find(user => user.login === userData.login)
    if (userExist) {
      // user exist
      return { status: 'exist' }
    } else {
      // add new user
      const token = getToken(userData)
      pushToStorage(usersArray, userData, token)
      return { status: 'registered', token: token }
    }
  }
}

export default userData => new Promise((resolve) => {
  // test errors
  // eslint-disable-next-line
  // func()
  setTimeout(() => resolve(resultFromServer(userData)), 500)
})
