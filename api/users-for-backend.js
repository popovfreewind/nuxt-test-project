export default () => {
  const usersInStorage = localStorage.getItem('users')
  return usersInStorage ? JSON.parse(usersInStorage) : []
}
