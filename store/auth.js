import registerUser from '../api/register-user'
import loginUser from '../api/login-user'
import logoutUser from '../api/logout-user'
import options from '../api/options'

export const state = () => ({
  authenticated: false,
  name: ''
})

export const mutations = {
  LOG_IN(state, name) {
    state.name = name
    state.authenticated = true
  },
  LOG_OUT(state) {
    state.name = ''
    state.authenticated = false
  }
}

export const actions = {
  async register(state, data) {
    try {
      const result = await registerUser(data)
      if (result.status === 'registered') {
        state.commit('LOG_IN', data.login)
        localStorage.setItem(options.tokenName, result.token)
        this.$router.push('/')
      } else {
        return result.status
      }
    } catch (e) {
      throw e
    }
  },

  async login(state, data) {
    try {
      const result = await loginUser(data)
      if (result.status === 'authenticated') {
        state.commit('LOG_IN', data.login)
        localStorage.setItem(options.tokenName, result.token)
        this.$router.push('/')
      } else {
        return result.status
      }
    } catch (e) {
      throw e
    }
  },

  async logout(state) {
    const logout = () => { // we can logout ewen on server error
      localStorage.removeItem(options.tokenName)
      state.commit('LOG_OUT')
      this.$router.push('/login')
    }
    try {
      const token = localStorage.getItem(options.tokenName)
      await logoutUser(token) // remove token from server
      logout()
    } catch (e) {
      console.error(e)
      logout()
    }
  }
}
