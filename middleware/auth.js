import checkUser from '../api/check-user'
export default async function ({ store, redirect }) {
  if (!store.state.auth.authenticated) {
    try {
      const authenticatedUser = await checkUser()
      if (authenticatedUser) {
        const login = authenticatedUser.login
        store.commit('auth/LOG_IN', login)
      } else return redirect('/login')
    } catch (e) {
      // on check error go to login page
      console.error(e)
      return redirect('/login')
    }
  }
}
